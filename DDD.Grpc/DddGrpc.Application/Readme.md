# Слой Application должен содержать:
* Команды записи (Command)
* Команды чтения (Query)
* Интерфейсы репозиториев для моделей чтения
* Интерфейсы внешних сервисов
* Exceptions
* Валидация
* Пайплайн запроса
* ДТО
* Маппинг сущностей

# Основная задача слоя Application
Координация действий, делегирование работы слою домена. Может содержать бизнес логику специфичную для приложения