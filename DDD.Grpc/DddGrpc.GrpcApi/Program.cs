using DddGrpc.Application;
using HardCode.Logging.Serilog;
using Serilog;
using DddGrpc.GrpcApi.Extensions;
using DddGrpc.Infrastructure;
using HardCode.Correlation;
using HardCode.Correlation.Grpc;
using HardCode.Correlation.Serilog;
using Microsoft.Extensions.Diagnostics.HealthChecks;

var builder = WebApplication.CreateBuilder(args);

const string applicationContext = "DddGrpc";

builder.Host.ConfigureAppSettings(builder.Environment.EnvironmentName);
builder.Host.UseSerilog(builder.Configuration.CreateSeriLogLogger(applicationContext));

builder.Services.AddApplicationLayer();
builder.Services.AddInfrastructureLayer(builder.Configuration);

builder.Services.AddGrpc(options =>
{
    options.Interceptors.Add<CorrelationIdInterceptor>();
});

builder.Services.AddHealthChecks()
    .AddNpgSql(
        npgsqlConnectionString: builder.Configuration.GetConnectionString("DbConnection"),
        name: $"{applicationContext} PostgreSQL Health Check",
        failureStatus: HealthStatus.Degraded,
        tags: new[] { "db", "postgresql", applicationContext });
    
var app = builder.Build();

app.SerilogAddBody();

// Очередность обязательна
app.UseCorrelationMiddleware();
app.UseCorrelationLogMiddleware();

app.MapHealthChecks("/health");

// Добавить grpc сервисы

await app.Services.MigrateDatabaseAsync();

app.Run();

// dotnet ef migrations add InitMigration --startup-project=DddGrpc.GrpcApi --project=DddGrpc.Infrastructure --context=AppDbContext
// dotnet ef database update --startup-project=DddGrpc.GrpcApi --project=DddGrpc.Infrastructure --context=AppDbContext