﻿using DddRest.Application.Services;
using HardCode.DomainDrivenDesign.Application.Common.Behaviors;
using HardCode.DomainDrivenDesign.Application.EventMappers;
using Microsoft.Extensions.DependencyInjection;

namespace DddRest.Application;

public static class ServiceRegistration
{
    public static void AddApplicationLayer(this IServiceCollection services)
    {
        var assembly = typeof(ServiceRegistration).Assembly;

        services.AddSingleton<IEventMapper, EventMapper>();

        services.AddAutoMapper(assembly);
        
        services.AddMediatR(x =>
        {
            x.RegisterServicesFromAssembly(assembly)
                .AddOpenBehavior(typeof(ValidationBehavior<,>))
                .AddOpenBehavior(typeof(LoggingBehavior<,>))
                .AddOpenBehavior(typeof(CacheBehavior<,>))
                .AddOpenBehavior(typeof(RequestPerformanceBehavior<,>))
                .AddOpenBehavior(typeof(TransactionalBehavior<,>));
        });
    }
}