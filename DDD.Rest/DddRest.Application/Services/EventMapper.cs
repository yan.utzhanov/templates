using HardCode.DomainDrivenDesign.Application.EventMappers;
using HardCode.IntegrationEvents.Contracts;
using MediatR;

namespace DddRest.Application.Services;

public class EventMapper : IEventMapper
{
    public IEnumerable<IIntegrationEvent> Map(INotification domainEvent)
    {
        return domainEvent switch
        {
            _ => Array.Empty<IIntegrationEvent>()
        };
    }
}