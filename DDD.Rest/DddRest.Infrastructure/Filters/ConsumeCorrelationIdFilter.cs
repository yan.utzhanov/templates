using HardCode.Correlation;
using HardCode.Correlation.Interfaces;
using MassTransit;
using LogContext = Serilog.Context.LogContext;

namespace DddRest.Infrastructure.Filters;

public class ConsumeCorrelationIdFilter<T> : IFilter<ConsumeContext<T>> where T : class
{
    private readonly ICorrelationService _correlationContext;

    public ConsumeCorrelationIdFilter(ICorrelationService correlationContext)
    {
        _correlationContext = correlationContext;
    }

    public async Task Send(ConsumeContext<T> context, IPipe<ConsumeContext<T>> next)
    {
        _correlationContext.SetCorrelationId(context.Headers.Get<Guid>(Constants.CorrelationIdHeaderName) ??
                                             Guid.Empty);

        using (LogContext.PushProperty("CorrelationId", _correlationContext.GetOrInitializeCorrelationId()))
        {
            await next.Send(context);
        }
    }

    public void Probe(ProbeContext context)
    {
    }
}