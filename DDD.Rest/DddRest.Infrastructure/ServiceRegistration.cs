﻿using DddRest.Infrastructure.Filters;
using DddRest.Infrastructure.Persistence;
using DddRest.Infrastructure.Services;
using HardCode.Correlation;
using HardCode.Correlation.Serilog;
using HardCode.DomainDrivenDesign.Application.Common;
using HardCode.DomainDrivenDesign.Infrastructure;
using HardCode.IntegrationEvents.Masstransit;
using HardCode.Services.Cache;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;

namespace DddRest.Infrastructure;

public static class ServiceRegistration
{
    public static void AddInfrastructureLayer(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<AppDbContext>(x => 
            x.UseNpgsql(configuration.GetConnectionString("DbConnection"))
                .UseLoggerFactory(NullLoggerFactory.Instance));
        services.AddScoped<DbContext>(x => x.GetRequiredService<AppDbContext>());

        services.AddHttpContextAccessor();

        services.AddBaseRepository();
        services.AddUnitOfWork<AppDbContext>();
        
        services.AddCorrelation();
        services.AddCorrelationIdLogDelegatingHandler();

        services.AddIntegrationEventPublisher();

        services.ConfigureHardCodeCache(
            Convert.ToBoolean(configuration.GetValue("Cache:UseRedis", false)),
            configuration.GetValue("Cache:ConnectionString", ""),
            configuration.GetValue("Cache:Name", "default")
        );

        services.AddSingleton<ICacheSettings, CacheSettings>();

        services.AddMassTransit(x =>
        {
            x.AddConsumersFromNamespaceContaining(typeof(ServiceRegistration));
            
            x.AddEntityFrameworkOutbox<AppDbContext>(o =>
            {
                o.UsePostgres();

                o.UseBusOutbox(s =>
                {
                    s.MessageDeliveryTimeout =
                        TimeSpan.FromSeconds(configuration.GetValue("Outbox:MessageDeliveryTimeout", 5));
                    
                    s.MessageDeliveryLimit = configuration.GetValue("Outbox:MessageDeliveryLimit", 100);
                });

                o.QueryDelay = TimeSpan.FromSeconds(configuration.GetValue("Outbox:QueryDelay", 30));
                o.QueryTimeout = TimeSpan.FromSeconds(configuration.GetValue("Outbox:QueryTimeOut", 10));

                if (configuration.GetValue("Outbox:DisableInboxCleanup", false))
                {
                    o.DisableInboxCleanupService();
                }
            });

            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host($"{configuration["RabbitMQ:HostName"]}", configuration["RabbitMQ:VirtualHost"], h =>
                {
                    h.Username(configuration["RabbitMQ:UserName"]);
                    h.Password(configuration["RabbitMQ:Password"]);
                });
               
                cfg.UseConsumeFilter(typeof(ConsumeCorrelationIdFilter<>), context);
                cfg.UsePublishFilter(typeof(SendCorrelationIdFilter<>), context);
                
                cfg.ConfigureEndpoints(context);
            });
        });
    }
    
    public static async Task MigrateDatabaseAsync(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
        await dbContext.Database.MigrateAsync();
    }
}