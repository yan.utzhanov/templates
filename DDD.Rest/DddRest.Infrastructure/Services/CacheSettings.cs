using HardCode.DomainDrivenDesign.Application.Common;
using Microsoft.Extensions.Configuration;

namespace DddRest.Infrastructure.Services;

public class CacheSettings : ICacheSettings
{
    public CacheSettings(IConfiguration configuration)
    {
        DurationInMinutes = configuration.GetValue("Cache:DurationInMinutes", defaultValue: 2);
    }

    public int DurationInMinutes { get; set; }
}