namespace DddRest.RestApi.Extensions;

public static class HostExtensions
{
    public static void ConfigureAppSettings(this IHostBuilder hostBuilder, string? env)
    {
        env ??= "Development";
        
        hostBuilder.ConfigureAppConfiguration(x =>
        {
            x.AddJsonFile("appsettings/appsettings.json", true, true)
                .AddJsonFile($"appsettings/appsettings.{env}.json", true, true);
        });
    }
}