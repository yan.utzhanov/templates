using HardCode.Correlation;
using HardCode.Correlation.Grpc;
using HardCode.Correlation.Serilog;
using HardCode.Logging.Serilog;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using ProcGrpc.Application;
using ProcGrpc.GrpcApi.Extensions;
using ProcGrpc.GrpcApi.Services;
using ProcGrpc.Infrastructure;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

const string applicationContext = "ProcGrpc";

builder.Host.ConfigureAppSettings(builder.Environment.EnvironmentName);
builder.Host.UseSerilog(builder.Configuration.CreateSeriLogLogger(applicationContext));

builder.Services.AddApplicationLayer();
builder.Services.AddInfrastructureLayer(builder.Configuration);

builder.Services.AddHealthChecks()
    .AddNpgSql(
        npgsqlConnectionString: builder.Configuration.GetConnectionString("DbConnection"),
        name: $"{applicationContext} PostgreSQL Health Check",
        failureStatus: HealthStatus.Degraded,
        tags: new[] { "db", "postgresql", applicationContext });

builder.Services.AddGrpc(options => { options.Interceptors.Add<CorrelationIdInterceptor>(); });

var app = builder.Build();

app.SerilogAddBody();

// Очередность обязательна
app.UseCorrelationMiddleware();
app.UseCorrelationLogMiddleware();

app.MapHealthChecks("/health");

app.MapGrpcService<GreeterService>();
app.MapGet("/",
    () =>
        "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

await app.Services.MigrateDatabaseAsync();

app.Run();

// dotnet ef migrations add InitMigration --startup-project=ProcGrpc.GrpcApi --project=ProcGrpc.Infrastructure --context=AppDbContext
// dotnet ef database update --startup-project=ProcGrpc.GrpcApi --project=ProcGrpc.Infrastructure --context=AppDbContext