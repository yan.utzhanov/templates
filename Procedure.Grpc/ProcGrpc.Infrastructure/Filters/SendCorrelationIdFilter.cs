using HardCode.Correlation;
using HardCode.Correlation.Interfaces;
using MassTransit;
using LogContext = Serilog.Context.LogContext;

namespace ProcGrpc.Infrastructure.Filters;

public class SendCorrelationIdFilter<T> : IFilter<PublishContext<T>> where T : class
{
    private readonly ICorrelationService _correlationContext;

    public SendCorrelationIdFilter(ICorrelationService correlationContext)
    {
        _correlationContext = correlationContext;
    }
    
    public async Task Send(PublishContext<T> context, IPipe<PublishContext<T>> next)
    {
        context.Headers.Set(Constants.CorrelationIdHeaderName, _correlationContext.GetOrInitializeCorrelationId(), false);
        
        using (LogContext.PushProperty("CorrelationId", _correlationContext.GetOrInitializeCorrelationId()))
        {
            await next.Send(context);
        }
    }

    public void Probe(ProbeContext context)
    {
    }
}