using HardCode.ProcedureDesign.Application;
using Microsoft.Extensions.DependencyInjection;

namespace ProcRest.Application;

public static class ServiceRegistration
{
    public static void AddApplicationLayer(this IServiceCollection services)
    {
        services.ConfigureGenericServices();
    }
}