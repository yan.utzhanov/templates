﻿using HardCode.Auth.Http;
using HardCode.Correlation;
using HardCode.Correlation.Serilog;
using HardCode.IntegrationEvents.Masstransit;
using HardCode.ProcedureDesign.Infrastructure;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;
using ProcRest.Infrastructure.Filters;
using ProcRest.Infrastructure.Persistence;

namespace ProcRest.Infrastructure;

public static class ServiceRegistration
{
    public static void AddInfrastructureLayer(this IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureDbContext<AppDbContext>(x => 
            x.UseNpgsql(configuration.GetConnectionString("DbConnection"))
                .UseLoggerFactory(NullLoggerFactory.Instance));

        services.ConfigureRepositories();
        services.AddIntegrationEventPublisher();
        
        services.AddHttpAuthContext();
        services.AddHttpContextAccessor();
        
        services.AddCorrelation();
        services.AddCorrelationIdLogDelegatingHandler();
        
        services.AddMassTransit(x =>
        {
            x.AddConsumersFromNamespaceContaining(typeof(ServiceRegistration));
            
            x.AddEntityFrameworkOutbox<AppDbContext>(o =>
            {
                o.UsePostgres();

                o.UseBusOutbox(s =>
                {
                    s.MessageDeliveryTimeout =
                        TimeSpan.FromSeconds(configuration.GetValue("Outbox:MessageDeliveryTimeout", 5));
                    
                    s.MessageDeliveryLimit = configuration.GetValue("Outbox:MessageDeliveryLimit", 100);
                });

                o.QueryDelay = TimeSpan.FromSeconds(configuration.GetValue("Outbox:QueryDelay", 30));
                o.QueryTimeout = TimeSpan.FromSeconds(configuration.GetValue("Outbox:QueryTimeOut", 10));

                if (configuration.GetValue("Outbox:DisableInboxCleanup", false))
                {
                    o.DisableInboxCleanupService();
                }
            });

            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host($"{configuration["RabbitMQ:HostName"]}", configuration["RabbitMQ:VirtualHost"], h =>
                {
                    h.Username(configuration["RabbitMQ:UserName"]);
                    h.Password(configuration["RabbitMQ:Password"]);
                });
               
                cfg.UseConsumeFilter(typeof(ConsumeCorrelationIdFilter<>), context);
                cfg.UsePublishFilter(typeof(SendCorrelationIdFilter<>), context);
                
                cfg.ConfigureEndpoints(context);
            });
        });
    }

    public static async Task MigrateDatabaseAsync(this IServiceProvider serviceProvider)
    {
        using var scope = serviceProvider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
        await dbContext.Database.MigrateAsync();
    }
}