using DddRest.RestApi.Extensions;
using HardCode.Correlation;
using HardCode.Correlation.Serilog;
using HardCode.Logging.Serilog;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using ProcRest.Application;
using ProcRest.Infrastructure;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

const string applicationContext = "ProcRest";

builder.Host.ConfigureAppSettings(builder.Environment.EnvironmentName);
builder.Host.UseSerilog(builder.Configuration.CreateSeriLogLogger(applicationContext));

builder.Services.AddApplicationLayer();
builder.Services.AddInfrastructureLayer(builder.Configuration);

builder.Services.AddHealthChecks()
    .AddNpgSql(
        npgsqlConnectionString: builder.Configuration.GetConnectionString("DbConnection"),
        name: $"{applicationContext} PostgreSQL Health Check",
        failureStatus: HealthStatus.Degraded,
        tags: new[] { "db", "postgresql", applicationContext });

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.SerilogAddBody();

// Очередность обязательна
app.UseCorrelationMiddleware();
app.UseCorrelationLogMiddleware();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

await app.Services.MigrateDatabaseAsync();

app.Run();

// dotnet ef migrations add InitMigration --startup-project=ProcRest.RestApi --project=ProcRest.Infrastructure --context=AppDbContext
// dotnet ef database update --startup-project=ProcRest.RestApi --project=ProcRest.Infrastructure --context=AppDbContext